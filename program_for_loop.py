# for, range, list
banyak = int(input("berapa banyak data ?"))

nama = []
umur = []

for i in range(0, banyak):
    print(f"data ke {i}")
    input_nama = input("masukkan nama :")
    input_umur = int(input("masukkan umur :"))
    
    nama.append(input_nama)
    umur.append(input_umur)

#print(nama)
#print(umur)

for i in range(0, len(nama)):
    data_nama = nama[i]
    data_umur = umur[i]
    print (f"{data_nama} umur {data_umur} tahun")
    