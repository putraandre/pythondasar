# dictionery python
# key:value 
# {}
pelanggan = {"nama":"rasja dinamit", "umur":23, "alamat":"repok bijang"}

# akses data
nama = pelanggan["nama"]
umur = pelanggan["umur"]
alamat = pelanggan["alamat"]
print (f"saya :{nama} berumur :{umur} dan beralamat :{alamat}")

# tambah data
pelanggan["jurusan"] = "teknik informatika"

# update data
pelanggan["nama"] = "heri"

# delete data
del pelanggan["jurusan"]

# menggunaka perulangan
for key in pelanggan:
    value = pelanggan[key]
    print(f"{key}:{value}")