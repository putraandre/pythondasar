# function parameter python
# def <function> (parameter):

def say_hello(name):
    print (f"hello : {name}")
 
say_hello("joko")

# function 2 or many argument
def hallo(first_name, last_name):
    print(f"hi : {first_name} {last_name}")
    
hallo("joko", "wicoyo")

# function default argument
def sapa(name="budi"):
    print(f"hi : {name}")

sapa("joko")
sapa()

# argument list
def jumlahkan(*list_angka):
    total = 0
    for angka in list_angka:
        total = total + angka
    print(f"total : {list_angka} = {total}")
    return total # return value
    
total = jumlahkan(10, 10, 10, 20)
print(total)