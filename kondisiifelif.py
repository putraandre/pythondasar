# if elif statment
from ast import If


hari_ini = "senin"

if (hari_ini == "senin"):
    print ("hari ini saya masuk kuliah")
elif (hari_ini == "selasa"):
    print ("hari ini saya masuk kuliah")
elif (hari_ini == "rabu"):
    print ("hari ini saya masuk kuliah")
elif (hari_ini == "kamis"):
    print ("hari ini saya masuk kuliah")
elif (hari_ini == "jumat"):
    print ("hari ini saya masuk kuliah")
elif (hari_ini == "sabtu"):
    print ("hari ini saya tidak kuliah")
elif (hari_ini == "minggu"):
    print ("hari ini saya libur kuliah")