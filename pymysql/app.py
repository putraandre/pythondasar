# program kasir menggunakan python dan mysql
import pymysql

# create connection
con = pymysql.connect(
    host = "localhost",
    user = "user",
    password = "1234",
    db = "kasir"
)
    
def show_data():
    cursor = con.cursor()
    query = "SELECT * FROM barang"
    cursor.execute(query)
    results = cursor.fetchall()
    
    if (cursor.rowcount < 0):
        print("Data Barang Tidak Tersedia")
    else:
        for data in results:
            print(f"Daftar Barang : {data}")
            
def insert_data():
    nama_barang = input("masukkan nama barang :")
    harga_barang = input("masukkan harga barang :")
    stok = input("masukkan stok barang :")
    
    cursor = con.cursor()
    query = "INSERT INTO barang (nama_barang, harga_barang, stok) VALUES (%s, %s,%s)"
    val = (nama_barang, harga_barang, stok)
    cursor.execute(query, val)
    con.commit()
    print("Berhasil Menambah Data Baru")
    
def update_data():
    cursor = con.cursor()
    show_data()
    
    id_barang = int(input("masukkan id_barang yang akan diupdate :"))
    nama_barang = input("masukkan nama barang :")
    harga_barang = input("masukkan harga barang :")
    stok = input("masukkan stok barang :")
    
    query = "UPDATE barang SET nama_barang = %s, harga_barang = %s, stok = %s WHERE id_barang = %s"
    val = (nama_barang, harga_barang, stok, id_barang)
    cursor.execute(query, val)
    con.commit()
    print("berhasil update data")
    
def delete_data():
    cursor = con.cursor()
    show_data()
    
    id_barang = int(input("masukkan id_barang yang akan dihapus :"))
    query = "DELETE FROM barang WHERE id_barang = %s"
    val = (id_barang)
    cursor.execute(query, val)
    con.commit()
    print("berhasil hapus data")
    
def search_data():
    cursor = con.cursor()
    kunci = input("masukkan nama barang yang akan dicari :")
    
    query = "SELECT * FROM barang WHERE id_barang LIKE %s OR nama_barang LIKE %s"
    val = ("%{}%".format(kunci), "%{}%".format(kunci))
    cursor.execute(query, val)
    results = cursor.fetchall()
    
    if cursor.rowcount < 0:
        print("data barang tidak ditemukan")
    else:
        for data in results:
            print(data)
            
while True:
    print("="*40)
    print(" WARUNG KELONTONG SANBA CERIA")
    
    print("[1] : Lihat Daftar Barang")
    print("[2] : Tambah Data Barang")
    print("[3] : Update Data Barang")
    print("[4] : Hapus  Data Barang")
    print("[5] : Cari   Data Barang")
    print("[6] : Kaluar")
    print("="*40)
    
    pilihan = input("masukkan pilihan :")
    
    if pilihan == "1":
        show_data()
    elif pilihan == "2":
        insert_data()
    elif pilihan == "3":
        update_data()
    elif pilihan == "4":
        delete_data()
    elif pilihan == "5":
        search_data()
    else:
        print("pilihan tidak tersedia")