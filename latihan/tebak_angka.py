import random
angka_rahasia = random.randint(1, 100)

print("=" * 40)
print("kami memiliki angka rahasia, silahkan tebak")
print("=" * 40)

batas_percobaan = 5
for percobaan in range(batas_percobaan):
    jawaban = int(input(f"[percobaan ke : {percobaan + 1}] masukkan angka :"))
    
    if jawaban == angka_rahasia:
        print("selamat tebakan kamu benar")
        break
    else:
        print(
            'Tebakanmu terlalu',
            'kecil' if jawaban < angka_rahasia else 'besar'
        )
else:
    print(f"sayang sekali kamu sudah salah menebak sebanyak : {percobaan}x!")