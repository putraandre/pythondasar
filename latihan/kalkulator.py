# kalkulator python
def tambah(a, b):
    return (a + b)

def kurang(a,b):
    return (a - b)

def kali(a, b):
    return (a * b)

def bagi(a, b):
    return (a / b)

def sisabagi(a , b):
    return (a % b)

while True:
    print("====KALKULATOR PYTHON====")
    print("1. Penjumlahan")
    print("2. Pengurangan")
    print("3. Perkalian")
    print("4. Pembagian")
    print("5. Sisa Bagi")
    
    pilihan = input("masukkan pilihan anda :")
    angka1  = int(input("masukkan angka pertama :"))
    angka2  = int(input("masukkan angka kedua :"))
    
    if pilihan == "1":
        print(f"{angka1} + {angka2} = ", tambah(angka1, angka2))
    elif pilihan == "2":
        print(f"{angka1} - {angka2}  = ", kurang(angka1, angka2))
    elif pilihan == "3":
        print(f"{angka1} * {angka2} = ", kali(angka1, angka2))
    elif pilihan == "4":
        print(f"{angka1} / {angka2} = ", bagi(angka1, angka2))
    elif pilihan == "5":
        print(f"{angka1} % {angka2} = ", sisabagi(angka1, angka2))
    else:
        print("pilihan yang anda masukkan tidak tersedia")