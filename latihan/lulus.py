tugas = float(input("masukkan nilai tugas :"))
uts = float(input("masukkan nilai uts :"))
uas = float(input("masukkan nilai uas :"))

nilai = (.15 * tugas) + (0.35 * uts) + (0.50 * uas)

if nilai > 80:
    grade = "A"
elif nilai > 70:
    grade = "B"
elif nilai > 60:
    grade = "C"
elif nilai > 50:
    grade = "D"
else:
    grade = "E"
   
if nilai > 60:
    status = "Lulus"
else:
    status = "Tidak Lulus"
    
print(f"nilai akhir : {nilai}")
print(f"grade : {grade}")
print(f"status : {status}")