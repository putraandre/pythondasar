def reverseString(x):
    # slice [:: – 1] mulai dari akhir string sampai posisi ke 0
    return x[::-1]

text = reverseString("Saya ingin tahu bagaimana teks ini terlihat terbalik")
print(text)

teks = "algoritma"
balikan = list(teks)
balikan.reverse()
print (''.join(balikan))