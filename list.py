buah = ["mangga", "apel", "jeruk", "manggis"]

# akses list
print (buah[2])

# tambah list
buah.append("melon")
buah.append("anggur")

# update list
buah[3] = "markisa"

# delete list
del buah[2]

for i in buah:
    print(f"saya suka makan buah : {i}")