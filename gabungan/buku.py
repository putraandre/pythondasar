# olah data buku menggunakan python
from textwrap import indent


buku = []

def show_data():
    if len(buku) <= 0:
        print("data buku tidak tersedia")
    else:
        for indeks in range(len(buku)):
            print("[%d] %s" % (indeks, buku[indeks]))
            
def insert_data():
    buku_baru = input("masukkan judul buku :")
    buku.append(buku_baru)
    
def update_data():
    show_data()
    indeks = int(input("Inputkan ID buku: "))
    
    if(indeks > len(buku)):
        print ("ID salah")
    else:
        judul_baru = input("Judul baru: ")
        buku[indeks] = judul_baru
        
def delete_data():
    show_data()
    indeks = input("masukkan id buku yang akan di hapus :")
    
    if (indeks > len(buku)):
        print("id buku salah")
    else:
        buku.remove(buku[indeks])
        
def show_menu():
    print ("----------- MENU ----------")
    print ("[1] Show Data")
    print ("[2] Insert Data")
    print ("[3] Edit Data")
    print ("[4] Delete Data")
    print ("[5] Exit")
    
    menu = input("PILIH MENU> ")
    
    if menu == "1":
        show_data()
    elif menu == "2":
        insert_data()
    elif menu == "3":
        update_data()
    elif menu == "4":
        delete_data()
    elif menu == "5":
        exit()
    else:
        print ("Salah pilih!")


if __name__ == "__main__":

    while(True):
        show_menu()