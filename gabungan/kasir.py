# program kasir menggunakan python
from dis import dis


print("="*40)
print(" SELAMAT DATANG DI MINIMARKET CERIA ")
print("="*40)

# menu
while True:
    print(" DAFTAR MENU ")
    print(" 1. Ayam Geprek Sambal : Rp 13.000")
    print(" 2. Nasi Goreng Kambing : Rp 17.000")
    print(" 3. Sate Ayam : Rp 20.000")
    print(" 4. Daging Rendang + Nasi : Rp 16.000")
    print(" 5. Pecel Lele Goreng + Nasi : Rp. 17.000")
    
    menuPilihan = input("masukkan menu pilihan anda :")
    jumlahPesanan = int(input("masukkan jumlah pesanan anda :"))
    
    if menuPilihan == "1":
        namaMenu = "Ayam Geprek Sambal"
        hargaMenu = 13.000 * jumlahPesanan
        pajak = hargaMenu * 0.1
        
        if jumlahPesanan >= 5:
            diskon = hargaMenu * 0.2
            totalHarga = hargaMenu - diskon + pajak
        else:
            diskon = 0
            totalHarga = hargaMenu + pajak
        
    elif menuPilihan == "2":
        namaMenu = "Nasi Goreng Kambing"
        hargaMenu = 17.000 * jumlahPesanan
        pajak = hargaMenu * 0.1
        
        if jumlahPesanan >= 5:
            diskon = hargaMenu * 0.2
            totalHarga = hargaMenu - diskon + pajak
        else:
            diskon = 0
            totalHarga = hargaMenu + pajak
        
    elif menuPilihan == "3":
        namaMenu = "Sate Ayam"
        hargaMenu = 20.000 * jumlahPesanan
        pajak = hargaMenu * 0.1
        
        if jumlahPesanan >= 5:
            diskon = hargaMenu * 0.2
            totalHarga = hargaMenu - diskon + pajak
        else:
            diskon = 0
            totalHarga = hargaMenu + pajak
        
    elif menuPilihan == "4":
        namaMenu = "Daging Rendang"
        hargaMenu = 16.000 * jumlahPesanan
        pajak = hargaMenu * 0.1
        
        if jumlahPesanan >= 5:
            totalHarga = hargaMenu + pajak
        else:
            totalHarga = hargaMenu + pajak
        
    elif menuPilihan == "5":
        namaMenu = "Pecel Lele Goreng + Nasi"
        hargaMenu = 16.000 * jumlahPesanan
        pajak = hargaMenu * 0.1
        
        if jumlahPesanan >= 5:
            totalHarga = hargaMenu + pajak
        else:
            totalHarga = hargaMenu + pajak
    
    else:
        print("menu tidak tersedia, silahkan pilih menu yang tersedia")
    
    
    print(" ------------------------------")
    print(" Menu :",namaMenu)
    print(" Jumlah Pesanan :", jumlahPesanan)
    print(" Harga :", hargaMenu)
    print(" Pajak :", pajak)
    print(" ------------------------------")
    print(" Total Pembayaran :", totalHarga)
    print(" ------------------------------")
    menu=input(" Mau pesan lagi? pilih Y jika Ya, pilih N jika Tidak (Y/N) = ")