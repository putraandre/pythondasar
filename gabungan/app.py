# program kasir sederhana
def counter_kasir():
    counter = input("hitung lagi ? (y/n)")
    
    if counter == "y":
        kasir()
    elif counter == "n":
        print("ingin hitung lagi ?")
        tanya()
    else:
        print("inputan salah ulangi lagi")
        
def kasir():
    nama_barang = input("masukkan nama barang :")
    harga = int(input("masukkan harga barang :"))
    jumlah = int(input("masukkan jumlah barang :"))
    
    total = harga * jumlah
    
    print(f"harga total : {nama_barang} = {total}")
    
    bayar = int(input("masukkan pembayaran :"))
    
    kurang = total - bayar
    kembalian = bayar - total
    
    if bayar > total:
        print(f"jumlah kembalian anda adalah :{kembalian}")
        tanya()
    elif bayar == total:
        print("uang anda pas, terima kasih")
    else:
        print("maaf uang anda tidak cukup {kurang}")
        counter_kasir()
        
def main_menu():
    print('=' * 10, 'MAIN MENU APLIKASI KASIR', '=' * 10)
    print('selamat datang di aplikasi kasir')
    print('=' * 20, 'masukan input aplikasi', '=' * 20)
    print('1. Program kasir')
    print('2. program kalkulator')
    print('3. exit program')
    
    pilihan = input("masukkan pilihan :")
    
    if pilihan == "1":
        kasir()
    elif pilihan == "2":
        kalkulator()
    else:
        print("program selesai")
        exit()
        
def get_login():
    print("="*20)
    print("halaman login kasir")
    username = input("masukkan username :")
    password = input("masukkan password :")
    
    if username == "admin" and password == "admin":
        print("login berhasil")
        main_menu()
    else:
        print("login gagal, username dan password salah")
        get_login()
        
def tanya():
    tanya = input("kembali ke menu ? (y/n)")
    
    if tanya == "y":
        main_menu()
    elif tanya == "n":
        exit()
    else:
        print("inputan salah")
        
def kalkulator():
    print("="*10)
    print("program kalkulator")
    print()
    print('Operator')
    print('=' * 10)
    print('1. tambah')
    print('2. kurang ')
    print('3. bagi')
    print('4. kali')
    print('5. sisa bagi/modulus')
    
    number1 = int(input("masukkan angka pertama :"))
    number2 = int(input("masukkan angka kedua :")) 
    operator = input("masukkan operator :")
    
    if operator == "1":
        print("hasil dari {} + {} = {}".format(number1, number2, number1+number2))
    elif operator == "2":
        print("hasil dari {} - {} = {}".format(number1, number2, number1-number2))
    elif operator == "3":
        print("hasil dari {} / {} = {}".format(number1, number2, number1/number2))
    elif operator == "4":
        print("hasil dari {} * {} = {}".format(number1, number2, number1*number2))
    elif operator == "5":
        print("hasil dari {} % {} = {}".format(number1, number2, number1%number2))
    else:
        print("operasi salah, silahkan ulangi")
        
if __name__ == "__main__":
    get_login()