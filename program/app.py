# managment kontak menggunakan python
import function

daftar_kontak = []
daftar_kontak.append({
    "nama" : "rendi putra",
    "email" : "batok@gmail.com",
    "telepon" : "081916686167"
})

while True:
    print("===DAFTAR MENU===")
    print("1. Lihat Kontak")
    print("2. Tambah Kontak")
    print("3. Hapus Kontak")
    print("4. Cari Kontak")
    print("0. Keluar")
    
    menu = input("pilih menu yang anda inginkan :")
    
    if menu == "0":
        break
    elif menu == "1":
        function.display_kontak(daftar_kontak)
    elif menu == "2":
        kontak = function.new_kontak()
        daftar_kontak.append(kontak)
    elif menu == "3":
        function.hapus_kontak(daftar_kontak)
    elif menu == "4":
        function.cari_kontak(daftar_kontak)
    else:
        print("pilihan tidak tersedia")
                
print ("program selesai, sampai jumpa lagi")