# managment kontak menggunakan python

import email


def display_kontak(daftar_kontak):
    for kontak in daftar_kontak:
        print(f"nama : {kontak['nama']}")
        print(f"email : {kontak['email']}")
        print(f"telepon : {kontak['telepon']}")
        
def new_kontak():
    nama = input("masukkan nama :")
    email = input("masukkan email :")
    telepon = input("masukkan telepon :")
    
    kontak = ({
        "nama" : nama,
        "email" : email,
        "telepon" : telepon
    })
    return kontak

def hapus_kontak(daftar_kontak):
    nama = input("masukkan nama kontak yang akan dihapus :")
    
    index = -1
    
    for i in range(0, len(daftar_kontak)):
        kontak = daftar_kontak[i]
        if nama == kontak["nama"]:
            index = i
            break
        
    if index == -1:
        print("nama kontak tidak ditemukan")
    else:
        del daftar_kontak[index]
        print("berhasil hapus kontak")
        
def cari_kontak(daftar_kontak):
    nama_yg_dicari = input("masukkan nama yang anda cari :")
    
    for kontak in daftar_kontak:
        nama = kontak["nama"]
        if nama.find(nama_yg_dicari) != -1:
            print("===DAFTAR KONTAK===")
            print(f"nama : {kontak['nama']}")
            print(f"email : {kontak['email']}")
            print(f"telepon : {kontak['telepon']}")